import sys

from GarbageBotMK2 import GitlabServer

if __name__ == '__main__':
    gls = GitlabServer()
    registry_repos = gls.get_registry_repositories_for_project()
    tag_name_list = []
    branch_slug_list = []
    image_tags_to_delete = []
    tag_list = []
    resp = None

    if gls.settings['dry_run']:
        print('#' * 10 + ' DRY RUN ' + '#' * 10)

    for reg in registry_repos:
        if gls.settings['gl_scope_registry']:
            tag_name_list.append(reg['name'])
        else:
            tag_list += gls.get_image_tags_for_registry_repository(reg['id'])
            for tag in tag_list:
                tag_name_list.append(tag['name'])
                tag['reg_id'] = reg['id']

    branches = gls.get_project_branches()
    for branch in branches:
        branch_slug_list.append(gls.normalize_branch_name(branch.attributes['name']))

    if gls.settings['gl_keep_branch_images']:
        image_tags_to_delete = set(tag_name_list) - set(branch_slug_list)
    elif gls.settings['gl_delete_by_regex']:
        image_tags_to_delete = gls.filter_taglist_regex(gls.settings['gl_delete_by_regex'], tag_list)
    elif gls.settings['gl_keep_by_regex']:
        image_tags_to_delete = gls.filter_taglist_regex(gls.settings['gl_keep_by_regex'], tag_list, keep=True)
    elif gls.settings['gl_images_to_delete']:
        image_tags_to_delete = gls.settings['gl_images_to_delete'].split(',')
    elif gls.settings['gl_images_to_keep']:
        image_tags_to_delete = set(tag_name_list) - set(gls.settings['gl_images_to_keep'].split(','))
    elif gls.settings['gl_delete_older_than']:
        for reg in registry_repos:
            gls.response_handling_del_request(response=gls.delete_bulk_image_tag(registry_id=reg['id'],
                                                                                 older_than=
                                                                                 gls.settings['gl_delete_older_than']),
                                              reg_repo_name=reg['location'], scope='bulk')
    elif gls.settings['purge_all']:
        for reg in registry_repos:
            gls.response_handling_del_request(response=gls.delete_single_registry_repository(registry_id=reg['id']),
                                              reg_repo_name=reg['location'],
                                              scope='bulk')

    image_tags_to_delete = list(filter(None, image_tags_to_delete))

    for it in image_tags_to_delete:
        print('Deleting image {0}'.format(it))
        if gls.settings['gl_scope_registry']:
            for reg in registry_repos:
                if reg['name'] == it:
                    gls.response_handling_del_request(
                        response=gls.delete_single_registry_repository(registry_id=reg['id']),
                        image_tag=it)
        else:
            for tag in tag_list:
                if tag['name'] == it:
                    gls.response_handling_del_request(response=gls.delete_single_image_tag(registry_id=tag['reg_id'],
                                                                                           tag_name=it),
                                                      image_tag=it)
    print('Done')
